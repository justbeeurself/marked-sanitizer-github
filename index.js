"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var htmlparser2_1 = require("htmlparser2");
var he_1 = require("he");
var voidElements = require("html-void-elements");
var VOID_ELEMENTS = new Set(voidElements);
var RE_EXTRACT_TAG_NAME = /<\/([^>]+)>/;
var HowToSanitize;
(function (HowToSanitize) {
    HowToSanitize[HowToSanitize["Escape"] = 0] = "Escape";
    HowToSanitize[HowToSanitize["Remove"] = 1] = "Remove";
    HowToSanitize[HowToSanitize["DoNothing"] = 2] = "DoNothing";
    HowToSanitize[HowToSanitize["EscapeWithoutPush"] = 3] = "EscapeWithoutPush";
})(HowToSanitize || (HowToSanitize = {}));
var isAbsolute = function isAbsolute(path) {
    return (/^(?:[a-z]+:)?\/\//i.test(path)
    );
};
var getProtocol = function getProtocol(path) {
    return path.split("/")[0].slice(0, -1);
};

var SanitizeWhitelist = function SanitizeWhitelist() {
    _classCallCheck(this, SanitizeWhitelist);

    this.ELEMENTS = new Set(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'h8', 'br', 'b', 'i', 'strong', 'em', 'a', 'pre', 'code', 'img', 'tt', 'div', 'ins', 'del', 'sup', 'sub', 'p', 'ol', 'ul', 'table', 'thead', 'tbody', 'tfoot', 'blockquote', 'dl', 'dt', 'dd', 'kbd', 'q', 'samp', 'var', 'hr', 'ruby', 'rt', 'rp', 'li', 'tr', 'td', 'th', 's', 'strike', 'summary', 'details']);
    this.REMOVE_CONTENTS = ['script'];
    this.ATTRIBUTES = {
        a: ['href'],
        img: ['src', 'longdesc'],
        div: ['itemscope', 'itemtype'],
        blockquote: ['cite'],
        del: ['cite'],
        ins: ['cite'],
        q: ['cite'],
        '*': new Set(['abbr', 'accept', 'accept-charset', 'accesskey', 'action', 'align', 'alt', 'axis', 'border', 'cellpadding', 'cellspacing', 'char', 'charoff', 'charset', 'checked', 'clear', 'cols', 'colspan', 'color', 'compact', 'coords', 'datetime', 'dir', 'disabled', 'enctype', 'for', 'frame', 'headers', 'height', 'hreflang', 'hspace', 'ismap', 'label', 'lang', 'maxlength', 'media', 'method', 'multiple', 'name', 'nohref', 'noshade', 'nowrap', 'open', 'prompt', 'readonly', 'rel', 'rev', 'rows', 'rowspan', 'rules', 'scope', 'selected', 'shape', 'size', 'span', 'start', 'summary', 'tabindex', 'target', 'title', 'type', 'usemap', 'valign', 'value', 'vspace', 'width', 'itemprop'])
    };
    // Note: Relative path also should be allowed
    this.PROTOCOLS = {
        a: {
            href: ['http', 'https', 'mailto', 'github-windows', 'github-mac']
        },
        blockquote: {
            cite: ['http', 'https']
        },
        del: {
            cite: ['http', 'https']
        },
        ins: {
            cite: ['http', 'https']
        },
        q: {
            cite: ['http', 'https']
        },
        img: {
            src: ['http', 'https'],
            longdesc: ['http', 'https']
        }
    };
};

exports.SanitizeWhitelist = SanitizeWhitelist;

var SanitizeConfig = function SanitizeConfig() {
    _classCallCheck(this, SanitizeConfig);

    this.LIST = ['ul', 'ol'];
    this.LIST_ITEM = 'li';
    this.TABLE_ITEMS = ['tr', 'td', 'th'];
    this.TABLE = 'table';
    this.TABLE_SECTIONS = ['thead', 'tbody', 'tfoot'];
    this.whitelist = new SanitizeWhitelist();
};

exports.SanitizeConfig = SanitizeConfig;

var SanitizeState = function () {
    function SanitizeState() {
        var _this = this;

        _classCallCheck(this, SanitizeState);

        this.config = new SanitizeConfig();
        this.onDetectedBroken = null;
        this.broken = false;
        this.tagStack = [];
        this.parser = new htmlparser2_1.Parser({
            onopentag: function onopentag(name, attrs) {
                _this.parsed = { name: name, attrs: attrs };
            },
            oncomment: function oncomment() {
                _this.parsed = null;
            }
        });
    }

    _createClass(SanitizeState, [{
        key: "reset",
        value: function reset() {
            this.tagStack = [];
            this.broken = false;
        }
    }, {
        key: "isInUse",
        value: function isInUse() {
            return this.broken || this.tagStack.length !== 0;
        }
    }, {
        key: "isBroken",
        value: function isBroken() {
            return this.broken;
        }
    }, {
        key: "getSanitizer",
        value: function getSanitizer() {
            return this.sanitize.bind(this);
        }
    }, {
        key: "sanitize",
        value: function sanitize(tag) {
            if (this.broken) {
                return he_1.escape(tag);
            }
            if (tag.startsWith('</')) {
                return this.sanitizeCloseTag(tag);
            } else {
                return this.sanitizeOpenTag(tag);
            }
        }
    }, {
        key: "itsBroken",
        value: function itsBroken(msg, tag) {
            if (this.broken) {
                // Already broken
                return;
            }
            this.broken = true;
            if (this.onDetectedBroken !== null) {
                this.onDetectedBroken(msg, tag);
            }
        }
    }, {
        key: "sanitizeCloseTag",
        value: function sanitizeCloseTag(tag) {
            var matched = tag.match(RE_EXTRACT_TAG_NAME);
            if (matched === null) {
                this.itsBroken("Closing HTML tag is broken: '" + tag + "'", tag);
                return he_1.escape(tag);
            }
            var tagName = matched[1].toLowerCase();
            if (VOID_ELEMENTS.has(tagName)) {
                return '';
            }
            if (!this.config.whitelist.ELEMENTS.has(tagName)) {
                // If tag name is not allowed, it is always escaped
                return he_1.escape(tag);
            }
            // Note: This check must be done after above void element check because tag history
            // stack is empty when a void element is at toplevel and with a closing tag.
            if (this.tagStack.length === 0) {
                this.itsBroken('Extra closing HTML tags in the document', tag);
                return he_1.escape(tag);
            }
            // Check top

            var _tagStack = _slicedToArray(this.tagStack[this.tagStack.length - 1], 2),
                name = _tagStack[0],
                how = _tagStack[1];

            if (tagName !== name) {
                this.itsBroken("Open/Closing HTML tag mismatch: </" + name + "> v.s. " + tag, tag);
                return he_1.escape(tag);
            }
            // Pop
            this.tagStack.pop();
            switch (how) {
                case HowToSanitize.Remove:
                    return '';
                case HowToSanitize.Escape:
                    return he_1.escape(tag);
                case HowToSanitize.DoNothing:
                    return tag;
                case HowToSanitize.EscapeWithoutPush:
                    throw new Error('NEVER REACH HERE');
            }
        }
    }, {
        key: "sanitizeOpenTag",
        value: function sanitizeOpenTag(tag) {
            var elem = this.parseOpenTag(tag);
            switch (elem) {
                case null:
                    // null means comment
                    return '';
                case undefined:
                    this.itsBroken("Failed to parse open HTML tag: '" + tag + "'", tag);
                    return he_1.escape(tag);
                default:
                    break;
            }
            var how = this.howToSanitize(elem);
            if (how !== HowToSanitize.EscapeWithoutPush && !tag.endsWith('/>') && !VOID_ELEMENTS.has(elem.name)) {
                // Note: If it's not an empty element, push history stack
                // Note: If the element is void element, we don't push it to tag hisotry stack.
                // On sanitizeCloseTag(), closing tags for void elements are simply skipped and they
                // never appear in converted HTML document.
                this.tagStack.push([elem.name, how]);
            }
            switch (how) {
                case HowToSanitize.Remove:
                    return '';
                case HowToSanitize.Escape:
                case HowToSanitize.EscapeWithoutPush:
                    return he_1.escape(tag);
                case HowToSanitize.DoNothing:
                    return tag;
            }
        }
    }, {
        key: "parseOpenTag",
        value: function parseOpenTag(tag) {
            this.parser.reset();
            this.parser.write(tag);
            this.parser.end();
            var parsed = this.parsed;
            this.parsed = undefined;
            return parsed;
        }
    }, {
        key: "howToSanitize",
        value: function howToSanitize(elem) {
            var _this2 = this;

            // Top-level <li> elements are removed because they can break out of
            // containing markup.
            if (elem.name === this.config.LIST_ITEM && this.tagStack.every(function (_ref) {
                var _ref2 = _slicedToArray(_ref, 2),
                    name = _ref2[0],
                    _ = _ref2[1];

                return _this2.config.LIST.indexOf(name) === -1;
            })) {
                return HowToSanitize.Remove;
            }
            // Table child elements that are not contained by a <table> are removed.
            if ((this.config.TABLE_SECTIONS.indexOf(elem.name) !== -1 || this.config.TABLE_ITEMS.indexOf(elem.name) !== -1) && this.tagStack.every(function (_ref3) {
                var _ref4 = _slicedToArray(_ref3, 2),
                    name = _ref4[0],
                    _ = _ref4[1];

                return _this2.config.TABLE !== name;
            })) {
                return HowToSanitize.Remove;
            }
            var wl = this.config.whitelist;
            // Check allowed (non escaped) elements
            if (!wl.ELEMENTS.has(elem.name)) {
                return HowToSanitize.EscapeWithoutPush;
            }
            // TODO: Check elements should be removed (not escaped, but just removed)
            // It's hard to remove content of the element with current markedjs implementation.
            var allowedAttrs = wl.ATTRIBUTES[elem.name];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = Object.keys(elem.attrs)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var attr = _step.value;

                    // Check allowed attributes
                    if (allowedAttrs !== undefined && allowedAttrs.indexOf(attr) === -1 && !wl.ATTRIBUTES['*'].has(attr)) {
                        return HowToSanitize.Escape;
                    }
                    // Check allowed protocols (e.g. 'href' of <a/>)
                    if (elem.name in wl.PROTOCOLS && attr in wl.PROTOCOLS[elem.name]) {
                        var value = elem.attrs[attr];
                        try {
                            var _ret = function () {
                                var protocol = getProtocol(value); // Omit last ':'
                                var allowedProtocols = wl.PROTOCOLS[elem.name][attr];
                                if (allowedProtocols.every(function (p) {
                                    return p !== protocol;
                                })) {
                                    return {
                                        v: HowToSanitize.Escape
                                    };
                                }
                            }();

                            if ((typeof _ret === "undefined" ? "undefined" : _typeof(_ret)) === "object") return _ret.v;
                        } catch (_) {
                            // Not a URL
                            if (isAbsolute(value)) {
                                return HowToSanitize.Escape;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return HowToSanitize.DoNothing;
        }
    }]);

    return SanitizeState;
}();

exports.default = SanitizeState;
//# sourceMappingURL=index.js.map